from django.db import models

class City(models.Model):
    name = models.CharField('City Name',max_length=250)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        
