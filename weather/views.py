from django.shortcuts import render
from django.http import HttpResponse
from .models import City
from .forms import CityModelForm
import requests
# Create your views here.

def index(request):
    
    url = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid=d6680cc08966a025592da0061765c7d3'

    if request.method == 'POST':
        form = CityModelForm(request.POST)
        form.save()
    else:
        pass
    
    form = CityModelForm()

    cities = City.objects.all()

    weather_data = []

    for city in cities:
        
        response = requests.get(url.format(city)).json()
       
        city_weather ={
            'city': city.name,
            'temperature': response['main']['temp'],
            'description': response['weather'][0]['description'],
            'icon': response['weather'][0]['icon'],
        }

        weather_data.append(city_weather)
    #print(weather_data)

    context = {'weather_data' : weather_data, 'form': form}
    
    return render(request, 'weather.html',context)